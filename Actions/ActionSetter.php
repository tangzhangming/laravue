<?php
namespace App\Admin\Actions;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ActionSetter
{

	private array $allow;

	private $callable;

	public function __call($function_name='', array $arguments)
	{
		return Str::startsWith($function_name, 'call_') ? $this->get($function_name, $arguments) : $this->set($function_name, $arguments) ;
	}

	public function set($function_name, array $arguments)
	{
		if( count($arguments) != 1 ){
			throw new \Exception("{$function_name} 参数必须且只能有一个");
		}
		if( is_callable($arguments[0]) === false ){
			throw new \Exception("{$function_name} 参数必须是一个闭包");
		}

		//检查是否允许

		return $this->callable[$function_name] = $arguments[0];
	}

	public function get($function_name, array $arguments)
	{
		$callname = substr($function_name, 5);
		if( Arr::get($this->callable, $callname) ){
			return call_user_func_array($this->callable[$callname], $arguments);
		}

		// 未设置略过
		return true;
	}

	/**
	 * 手工调用配置器
	 */
	public function call($name, ...$arguments)
	{
		if( Arr::get($this->callable, $name) ){
			return call_user_func_array($this->callable[$name], $arguments);
		}
	}

	public function has($name)
	{
		return Arr::has($this->callable, $name);
	}

	
}