<?php
namespace App\Admin;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;


class Helper
{
	
	public static function resource($name, $controller)
	{
		// Route::get("{$name}/create", "{$controller}@save");
		// Route::get("{$name}/delete", "{$controller}@delete");
		Route::get("{$name}/restore", "{$controller}@restore");

		// 列表
		Route::get($name, "{$controller}@index");

		// 创建
		Route::post($name, "{$controller}@save");

		// 更新
		Route::put("{$name}/{id}", "{$controller}@update");

		// 查询
		Route::get("{$name}/{id}", "{$controller}@show");

		// 删除[批量]
		Route::delete("{$name}/destroy", "{$controller}@destroy");

		// 垃圾站
		Route::get("{$name}/trashed", "{$controller}@trashed");

		// 恢复[批量]
		Route::post("{$name}/restore", "{$controller}@restore");

		// 删除[批量]
		Route::delete("{$name}/delete", "{$controller}@delete");
	}


	public static function buildTree($items, $parentId = null, $mk='id', $pk='parent_id', $call=null) {
	    $branch = [];

	    if( is_null($call) ){
	    	$call = function($item){
	    		return $item;
	    	};
	    }

	    foreach ($items as $item) {
	        // 检查当前项的parent_id是否与传入的parentId匹配
	        if ($item[$pk] == $parentId) {
	            // 递归构建子树
	            // dd(Arr::get($item, 'id'));
	            $pid = Arr::get($item, $mk);
	            $children = self::buildTree($items, $pid, $mk, $pk, $call);
	            if (!empty($children)) {
	                $item['children'] = $children;
	            }
	            $branch[] = $call($item);
	        }
	    }

	    return $branch;
	}
}
