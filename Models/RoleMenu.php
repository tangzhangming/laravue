<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{

    public $timestamps = false;

    protected $table = 'system_role_menu';

    protected $fillable = ['role_id', 'menu_id'];

}