<?php
namespace App\Admin\Models;


use Illuminate\Support\Arr;

class Token
{
	private $token;

	private $info;
	
	function __construct(string $token, string $value)
	{
		$this->token = $token;
		$this->info = json_decode($value, true);
	}

	public static function make(string $token, string $value)
	{
		return (new self($token, $value));
	}

	public function getIpAddress()
	{
		return Arr::get($this->info, 'ip_address');;
	}

	public function getUserAgentAddress()
	{
		return Arr::get($this->info, 'user_agent');;
	}

    public function toArray()
    {
        return [
        	'token' => $this->token,
        	'ip_address' => $this->getIpAddress(),
        	'user_agent' => $this->getUserAgentAddress(),
        ];
    }
}