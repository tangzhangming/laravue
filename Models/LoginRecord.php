<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class LoginRecord extends Model
{

    public $timestamps = false;

    protected $table = 'system_login_record';

    protected $appends = ['os', 'browser'];

    /**
     * List of additional operating systems.
     *
     * @var array<string, string>
     */
    protected static $additionalOperatingSystems = [
        'Windows' => 'Windows',
        'Windows NT' => 'Windows NT',
        'OS X' => 'Mac OS X',
        'Debian' => 'Debian',
        'Ubuntu' => 'Ubuntu',
        'Macintosh' => 'PPC',
        'OpenBSD' => 'OpenBSD',
        'Linux' => 'Linux',
        'ChromeOS' => 'CrOS',
    ];

    /**
     * List of additional browsers.
     *
     * @var array<string, string>
     */
    protected static $additionalBrowsers = [
        'Opera Mini' => 'Opera Mini',
        'Opera' => 'Opera|OPR',
        'Edge' => 'Edge|Edg',
        'Coc Coc' => 'coc_coc_browser',
        'UCBrowser' => 'UCBrowser',
        'Vivaldi' => 'Vivaldi',
        'Chrome' => 'Chrome',
        'Firefox' => 'Firefox',
        'Safari' => 'Safari',
        'IE' => 'MSIE|IEMobile|MSIEMobile|Trident/[.0-9]+',
        'Netscape' => 'Netscape',
        'Mozilla' => 'Mozilla',
        'WeChat' => 'MicroMessenger',
    ];

    protected function getOsAttribute()
    {
        foreach (static::$additionalOperatingSystems as $key => $value) {
            if (stripos($this->user_agent, $key) !== false) {
                return $value;
            }
        }
    }

    protected function getBrowserAttribute()
    {
        foreach (static::$additionalBrowsers as $key => $value) {
            if (stripos($this->user_agent, $key) !== false) {
                return $value;
            }
        }
    }
}