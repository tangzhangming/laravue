<?php
namespace App\Admin\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'system_role';

    // 可以批量赋值属性
    protected $fillable = ['name', 'code', 'sort', 'status'];

    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(Menu::class, 'system_role_menu', 'role_id', 'menu_id');
    }

    public function getMenuIdsAttribute()
    {
        return $this->menus->pluck('id');
    }

}
