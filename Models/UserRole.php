<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    public $timestamps = false;

    protected $table = 'system_user_role';

    protected $fillable = ['user_id', 'role_id'];

}