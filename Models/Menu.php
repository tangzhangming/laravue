<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $table = 'system_menu';

    // 可以批量赋值属性
    protected $fillable = [
        'parent_id', 
        'type', 
        'title', 
        'icon',
        'name', 
        'path',
        'component',
        'is_hidden',
        'sort'
    ];


    public function getButtons(array $buttonKeys)
    {
        $buttons = [
            'store'   => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '新增', 'name'  => "{$this->name}::store"],
            'edit'    => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '编辑', 'name'  => "{$this->name}::edit"],
            'destroy' => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '删除', 'name'  => "{$this->name}::destroy"],
            'trashed' => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '回收站', 'name'  => "{$this->name}::trashed"],
            'restore' => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '恢复数据', 'name'  => "{$this->name}::restore"],
            'delete'  => ['parent_id' => $this->id, 'type'  => 'B', 'title' => '物理删除', 'name'  => "{$this->name}::delete"],
        ];

        $newButtons = [];
        foreach ($buttons as $key => $value) {
            if( in_array($key, $buttonKeys) ){
                $newButtons[] = $value;
            }
        }

        return $newButtons;
    }
}
