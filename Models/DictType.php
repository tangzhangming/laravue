<?php
namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DictType extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'system_dict_type';

    // 可以批量赋值属性
    protected $fillable = ['code', 'name', 'status'];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }
}
