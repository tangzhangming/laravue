<?php
namespace App\Admin\Controllers\System;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Admin\Admin;
use App\Admin\Controllers\Controller;
use App\Admin\Models\Role;
use App\Admin\Requests\SystemRoleRequest;

use App\Admin\Actions\DataTable;
use App\Admin\Actions\SoftDelete;
use App\Admin\Actions\Delete;

class RoleController extends Controller
{
	use DataTable, SoftDelete, Delete;

	public function modelName()
	{
		return Role::class;
	}

	public function dataTable($setter)
	{
		$setter->validate(function($request){
			return $request->validate([
        		'name'=>'nullable|string'
			]);
		});
		$setter->sort(function($query){
			return $query->orderBy('sort');
		});

		$setter->search(function($query, $validated){
			if( !empty($validated['name']) ){
				$query->where('name', $validated['name']);
			}
			if( !empty($validated['code']) ){
				$query->where('code', $validated['code
					']);
			}
		});
	}

	public function show(Request $request, $id)
	{
		$model = Role::findOrFail($id);
		$model->append(['menu_ids'])->makeHidden('menus');

		return Admin::success($model);
	}

	public function store(Request $request){
		$validated = $request->validate([
            'name'     => ['sometimes', 'alpha_num', 'between:2,50'],
            'code'     => ['sometimes', 'alpha_num:ascii', 'between:2,50'],
            'sort'     => 'sometimes|integer|min:0',
            'status'   => 'sometimes|integer|accepted_if:id,1',
            // 附加字段
            'menu_ids'   => ['nullable', 'array'],
            'menu_ids.*'   => ['sometimes', 'integer', 'exists:system_menu,id'],
		]);
		
		$model = Role::create($validated);
		// $model?->menus()?->sync($validated['menu_ids']);

		return Admin::success($model);
	}

	public function update(Request $request, $id){
		$unique = Rule::unique('system_role')->ignore($id);
		$validated = $request->validate([
            'name'     => ['sometimes', 'alpha_num', 'between:2,50', $unique],
            'code'     => ['sometimes', 'alpha_num:ascii', 'between:2,50', $unique],
            'sort'     => 'sometimes|integer|min:0',
            'status'   => 'sometimes|integer|accepted_if:id,1',
            // 附加字段
            'menu_ids'   => ['nullable', 'array'],
            'menu_ids.*'   => ['sometimes', 'integer', 'exists:system_menu,id'],
		]);

		$model = Role::findOrFail($id);
		$model->fill($validated);
		$isDirty = $model->isDirty();
		if( $isDirty ){
			$model->save();
		}

		if( isset($validated['menu_ids']) ){
			$model?->menus()->sync($validated['menu_ids']);
		}

		return Admin::success($isDirty);
	}



	public function actionDelete($setter)
	{
		$setter->handle(function($eloquent){
			if( $eloquent instanceof \Illuminate\Database\Eloquent\Model ){
				if( $eloquent->id == 1 ){
					abort(403, '超级管理员角色不能被删除');
				}
				$eloquent->delete();

			}else{
				$eloquent->each(function($model){
					if( $model->id == 1 ){
						abort(403, '超级管理员角色不能被删除');
					}
					$model->delete();
				});	
			}
		});
	}



	public function select(Request $request)
	{
		$models = Role::select('name as label', 'id as value')
					->limit(100)
					->get();

		return $this->success($models);
	}
}
