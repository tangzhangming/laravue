<?php
namespace App\Admin\Controllers\Auth;

use GeoIp2\Database\Reader;
use Illuminate\Http\Request;
use App\Admin\Controllers\Controller;
use App\Admin\Admin;
use App\Admin\Models\LoginRecord;

class LoginRecordController extends Controller
{
	// http://www.llc.com/api/admin/system/login-record
	public function index(Request $request)
	{
		$model = LoginRecord::query()->first();

		return Admin::success($model);
	}


	/**
	 * 销毁Token[退出]
	 */
	public function destroy(Request $request)
	{
		return $this->success('destroy');
	}

}
