<?php
namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Attachment;
use App\Admin\Actions\DataTable;
use App\Admin\Actions\SoftDelete;
use App\Admin\Actions\Delete;

class AttachmentController extends Controller
{
	use DataTable, SoftDelete, Delete;

	public function modelName()
	{
		return Attachment::class;
	}

	public function dataTable($setter)
	{
		$setter->validate(function($request){
			return $request->validate([
				'origin_name'  => 'nullable|string',
				'mime_type'    => 'nullable|string|max:50',
				'created_at'   => 'nullable|array|size:2',
				'created_at.0' => 'sometimes|date_format:Y-m-d',
				'created_at.1' => 'sometimes|date_format:Y-m-d',
			]);
		});

		$setter->search(function($query, $validated){
			if( isset($validated['origin_name']) && $validated['origin_name']!='' ){
				$query->where('origin_name', $validated['origin_name']);
			}
			if( isset($validated['mime_type'])  ){
				$query->where('mime_type', $validated['mime_type']);
			}
			if( isset($validated['created_at'])  ){
				list($start, $end) = $validated['created_at'];
				$query->whereBetween('created_at', [$start." 00:00:00", $end." 23:59:59"]);
			}
		});
	}

}
