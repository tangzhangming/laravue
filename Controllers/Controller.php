<?php
namespace App\Admin\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use ValidatesRequests;

    // public function auth()
    // {
    //     return Auth::guard('admin');
    // }

    public function success($data=[], $code=0, $message='success')
    {
        return [
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ];
    }
}
