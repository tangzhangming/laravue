<?php
namespace App\Admin\Requests\System;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;
use App\Admin\Models\User;

class UserCreateRequest extends FormRequest
{
    use PasswordValidationRules;

    /**
     * 邮箱和手机必须至少设置一项用于登录
     */
    public function rules(): array
    {
        return [
            'email'     => [
                'required_without:mobile',
                'nullable',
                'email', 
                'unique:system_user', 
            ],
            'mobile'     => [
                'required_without:email',
                'nullable', 
                'digits:11', 
                'unique:system_user'
            ],
            'name'      => [
                'required',
                'alpha_num',
                'between:2,20',
            ],
            'password'  => $this->passwordRules(),
            'password'  => ['required', 'string', Password::default()],
            'profile_photo_path'    => [
                'nullable', 
                // 'url',
            ],
            'status'    => [
                'required', 
                'in:0,1',
            ],

            //附加字段
            'role_ids'   => ['nullable', 'array'],
            'role_ids.*'   => ['sometimes', 'integer', 'exists:system_role,id'],
        ];
    }

    public function attributes()
    {
        return [
            'email' => '邮箱',
            'mobile' => '手机号',
        ];
    }

    public function messages()
    {
        return [
            'email.required_without' => ':attribute和:values必须至少设置一项',
            'mobile.required_without' => ':attribute和:values必须至少设置一项',
        ];
    }
}
