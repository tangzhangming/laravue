<?php
namespace App\Admin\Requests;

use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;
use App\Admin\Models\User;

class MenuUpdateRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'login_name' => 'required|between:5,50',
            'password'   => 'required|between:6,20',
        ];
    }

    public function attributes()
    {
        return [
            'login_name' => '账号',
            'password'   => '密码',
        ];
    }

    public function messages()
    {
        return [
            'login_name.required' => ':attribute不能为空',
            'login_name.regex'    => '仅支持使用电子邮箱或手机号登录',

            'password.required' => ':attribute不能为空',
            'password.between' => ':attribute长度不符合要求，请检查是否正确',
            'password.regex'   => ':attribute错误',
        ];
    }
}
