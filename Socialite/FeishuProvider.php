<?php
namespace App\Admin\Socialite;

use GuzzleHttp\RequestOptions;
use Laravel\Socialite\Two\User;
use Laravel\Socialite\Two\AbstractProvider;
use Illuminate\Support\Arr;
use Illuminate\Http\RedirectResponse;


class FeishuProvider extends AbstractProvider
{

    protected function getAuthUrl($state){
    	return $this->buildAuthUrlFromBase('https://open.feishu.cn/open-apis/authen/v1/authorize', $state);
    }

    protected function getCodeFields($state = null)
    {
        $fields = parent::getCodeFields($state);

        $fields['app_id'] = $fields['client_id'];
        unset($fields['client_id']);

        return $fields;
    }



    protected function getTokenUrl(){
        return 'https://open.feishu.cn/open-apis/authen/v1/oidc/access_token';
    }

    // https://open.feishu.cn/document/server-docs/authentication-management/login-state-management/get?appId=cli_a6c6d160afbb500d
    protected function getUserByToken($token){
        $userUrl = 'https://open.feishu.cn/open-apis/authen/v1/user_info';

        $response = $this->getHttpClient()->get($userUrl, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer '.$token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    protected function mapUserToObject(array $user){
        return (new User)->setRaw($user)->map([
            // 'id'     => Arr::get($user, 'user_id'),
            // 'avatar' => Arr::get($user, 'avatar_url'),
            // 'name'   => Arr::get($user, 'name'),
            // 'email'  => Arr::get($user, 'email'),
            // 'mobile' => Arr::get($user, 'mobile'),
        ]);
    }


    /**
     * 飞书奇葩 先拿app_access_token换user_access_token
     * https://open.feishu.cn/document/server-docs/authentication-management/access-token/app_access_token_internal
     * https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/authen-v1/oidc-access_token/create
     */
    public function getAccessTokenResponse($code)
    {
        $url = 'https://open.feishu.cn/open-apis/auth/v3/app_access_token/internal';
        $response = $this->getHttpClient()->post($url, [
            RequestOptions::HEADERS => [
            	// 'Content-Type' => 'application/json; charset=utf-8',
                'Content-Type' => 'application/json',
            ],
            RequestOptions::FORM_PARAMS => [
            	'app_id'     => 'cli_a6c6d160afbb500d',
            	'app_secret' => 'Q6pRXlWo2RhORPVNn8YPqbngyyoZqFi5',
            ],
        ]);
        $response = json_decode($response->getBody(), true);
        // $app_access_token = json_decode($response->getBody(), true);

        dd($response);







        // $response = $this->getHttpClient()->post($this->getTokenUrl(), [
        //     RequestOptions::HEADERS => [
        //         'Authorization' => 'Bearer t-g1045p1I727LJPRDEVA6M2MSHS5POQMW4SIX4P4D',
        //     	'Content-Type' => 'application/json',
        //     ],
        //     RequestOptions::FORM_PARAMS => [
        //     	'grant_type' => 'authorization_code',
        //     	'code' => $code,
        //     ],
        // ]);

        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer t-g1045p1I727LJPRDEVA6M2MSHS5POQMW4SIX4P4D',
             'Content-Type' => 'application/json',
            ],
            RequestOptions::FORM_PARAMS => [
             'grant_type' => 'authorization_code',
             'code' => $code,
            ],
        ]);

dd( json_decode($response->getBody(), true) );
        return json_decode($response->getBody(), true);
    }

//     public function user()
//     {
//         if ($this->user) {
//             return $this->user;
//         }

//         if ($this->hasInvalidState()) {
//             throw new InvalidStateException;
//         }


//         $user = $this->getUserByToken('u-fMfBDLU1pdAGMi.KjCTZtkh00opg14dFV80015S00aSd');
// dd($user);
//         return $this->userInstance($response, $user);
//     }

}